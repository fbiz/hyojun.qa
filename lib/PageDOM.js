module.exports = function(html){
	var $ = html;

	$.getPathTo = function(selector){
		var elm = $(selector);

		if (elm.attr('id') && elm.attr('id') !== '') {
			return elm[0].name.toLowerCase() + '#' + elm.attr('id');
		}
		if (elm.is('html') || elm.is('head') || elm.is('body')) {
			return elm[0].name.toLowerCase();
		}
		var idx = 0;
		var siblings = elm.parent().children();

		for (var i=0; i<siblings.length; ++i){
			var sibling = $(siblings[i]);
			if (sibling[0] === elm[0]) {
				return $.getPathTo(elm.parent())+' > '+elm[0].name.toLowerCase()+'['+(idx)+']';
			}
			if (sibling[0].type.toLowerCase() === 'tag' && sibling[0].name.toLowerCase() === elm[0].name.toLowerCase()) {
				idx++;
			}
		}
	};

	$.doctype = function(){
		var htmlText = $.html(),
			matches,
			doctypes = [],
			dtRegExp = /<(\s?)+(!doctype)([^>]+)(\s?)+>/igm;

		while ((matches = dtRegExp.exec(htmlText))){
			doctypes.push(matches[0]);
		}

		return doctypes;
	};

	return $;
};