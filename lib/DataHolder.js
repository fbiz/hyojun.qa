var DataHolder = function(){
	var data = {};

	this.ua = null;
	this.defaults = null;

	this.init = function(){
		this.ua = require('../data/hqa.user-agents.json');
		this.defaults = require('../data/hqa.defaults.json');

		for (var prop in this.defaults){
			if (this.defaults.hasOwnProperty(prop)) {
				var val = this.defaults[prop];

				if (prop.toLowerCase() === 'user-agent'){
					val = this.propOrValue(this.defaults[prop], this.ua);
				}

				data[prop] = val;
			}
		}
	};

	this.getData = function(){
		return data;
	};

	this.get = function(prop){
		return data[prop];
	};

	this.set = function(prop, value){
		data[prop] = value;
	};

	this.propOrValue = function(prop, obj){
		var p = this.propInObj(prop, obj);

		return p !== false ? p : prop;
	};

	this.propInObj = function(prop, obj){
		for (var p in obj){
			if (p.toLowerCase() === prop.toLowerCase()){
				return obj[p];
			}
		}

		return false;
	};

	this.parseData = function(obj){
		for (var prop in obj){
			if (obj.hasOwnProperty(prop)) {
				var val = obj[prop];

				if (prop.toLowerCase() === 'user-agent'){
					val = this.propOrValue(this.defaults[prop], this.ua);
					obj[prop] = val;
				}
			}
		}

		return obj;
	};
};

module.exports = DataHolder;