var fs = require('fs'),
	path = require('path'),
	merge = require('mout/object/merge');

var CONFIG_PATH = './hqa.config.js',
	URLS_PATH = './hqa.urls.js',
	TESTS_PATH = './hqa-tests';

function findPath(filePath, defaultPath) {
	var baseFile = path.basename(filePath || ''),
		baseFolder = path.dirname(filePath || ''),
		defaultFile = path.basename(defaultPath || ''),
		defaultFolder = path.dirname(defaultPath || ''),
		result;

	function checkPath(filePath, baseFolder, baseFile) {
		var folder,
			levels = [];
		if (!filePath) {
			return null;
		}
		if (fs.existsSync(filePath)) {
			return filePath;
		}
		levels.length = (path.join(baseFolder, baseFile).split(path.sep).length || 1) + 1;
		folder = path.resolve(filePath, levels.join('../'));
		if (folder === path.resolve('/')) {
			return null;
		}
		return checkPath(path.resolve(folder, '..', baseFolder, baseFile), baseFolder, baseFile);
	}

	// will load the asked file, else, will try the default path
	result = checkPath(filePath, baseFolder, baseFile);
	if (!result) {
		result = checkPath(defaultPath, defaultFolder, defaultFile);
	}

	return result ? path.resolve(result) : null;
}

function Config(data) {

	this.data = data || {};

	if (this.data.path) {
		this.init(this.data.path);
	}
}

Config.prototype = {
	init: function(configPath){
		this.setPath(configPath);
		this.load();
	},
	setPath: function(configPath) {
		this.data.path = configPath || this.data.path;
		if (path.extname(this.data.path) !== '.js'){
			this.data.path = CONFIG_PATH;
		}
	},
	get: function(val) {
		return this.data[val];
	},
	set: function(prop, val) {
		this.data[prop] = val;
	},
	load: function() {

		var path = findPath(this.data.path, CONFIG_PATH);
		if (!path) {
			throw new Error(['Config file not found.',
				'Place "hqa.config.js" on the root of your project or',
				'point it with --config parameter.',
				'\nAborting.'].join(' '));
		}

		this.data.path = path;
		this.data = merge({}, this.data, require(path));

		// check config special sections
		this.checkURLList();
		this.checkTestsLocation();
	},
	checkURLList: function() {
		var data;
		this.data.urlPath = null;
		if (!this.data.urlList || typeof this.data.urlList === 'string') {
			data = findPath(this.data.urlList, URLS_PATH);
			if (data) {
				this.data.urlPath = data;
				this.data = merge(this.data, require(data));
			}
		}
		if (!this.data.urlList) {
			throw new Error(['URL list not defined. Aborting.',
				'Place "hqa.urls.js" on the root of your project,',
				'point it inside your config file or',
				'use --url parameter.',
				'\nAborting.'].join(' '));
		}
	},
	checkTestsLocation: function(list) {

		var testOrder = this.data.testOrder,
			testList = [];

		this.data.testsToLoad = [];
		this.data.testPath = null;

		if (!list && (!testOrder || typeof testOrder === 'string')) {
			testOrder = findPath(this.data.testOrder, TESTS_PATH);
			if (testOrder) {
				this.data.testPath = testOrder;
				testList = fs.readdirSync(testOrder) || [];
				testList.forEach(function(val) {
					var filePath = path.resolve(testOrder, val);
					if (fs.existsSync(filePath)){
						this.data.testsToLoad.push(filePath);
					}
				}.bind(this));
			}
		} else {
			testList = list || testOrder;
			testList.forEach(function(val) {
				if (fs.existsSync(val)){
					this.data.testsToLoad.push(path.resolve(val));
				}
			}.bind(this));
		}
	}
};

module.exports = Config;