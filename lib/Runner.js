var cheerio = require('cheerio');
var clc = require('cli-color');
var events = require('events');
var util = require ('util');

var PageDOM = require('./PageDOM.js');
var Test = require('./Test.js');

var Runner = function(request, config, dataObj, cookieJar){
	this.request = request;
	this.dataObj = dataObj;
	this.authCookies = cookieJar;
	this.testsToLoad = config.get('testsToLoad');
	this.urlList = [];
	this.totalPasses = 0;
	this.totalFails = 0;

	this.init = function(urlList){
		this.urlList = urlList;

		this.on('run', this.run);

		var u = this.urlList.shift();
		this.openAndRunTests(u);
	};

	this.run = function(){
		var _self = this;

		if (_self.urlList.length === 0){
			_self.emit('finish', {totalPasses: _self.totalPasses, totalFails: _self.totalFails});
			return;
		}

		var u = _self.urlList.shift();
		_self.openAndRunTests(u);
	};

	this.openAndRunTests = function(urlItem){
		var _self = this;

		var use_auth = urlItem.auth;

		var options = {
			url: urlItem.url,
			headers: {
				'user-agent' : _self.dataObj.get('user-agent')
			}
		};

		if (urlItem.postData){
			options.form = urlItem.postData;
			options.method = 'POST';
		}

		if (urlItem.headers){
			options.headers = _self.dataObj.parseData(urlItem.headers);
		}

		var auth = _self.dataObj.get('auth');

		if (auth && use_auth) {
			options.jar = _self.authCookies;
		}

		if ((auth && use_auth) || (!auth && !use_auth) || !use_auth){
			_self.request(options, function(error, response, body){
				if (!error && response.statusCode === 200) {
					if (_self.dataObj.get('loglevel') > 1){
						console.log(clc.cyanBright.bold('---'));
						console.log(clc.cyanBright.bold('URL: ' + urlItem.url) + (use_auth ? ' - (auth)' : ')') + '\n');
					}
					_self.runAssertTests(response, body);
				} else {
					if (error){
						console.log(clc.redBright('ERROR '+error+': '+urlItem.url));
					}
					if (response && response.statusCode !== 200){
						console.log(clc.redBright('HTTP ERROR '+response.statusCode+': '+urlItem.url));
					}
					
				}
				
				_self.emit('run');
			});
		} else {
			_self.emit('run');
		}
	};

	this.runAssertTests = function(response, body){
		var _self = this;
		var pageDOM = new PageDOM(cheerio.load(body));

		var testconfig = {
			loglevel: _self.dataObj.get('loglevel') || 1,
			failonly: _self.dataObj.get('failonly') || false
		};

		var test = new Test(pageDOM, testconfig);

		for (var i=0; _self.testsToLoad[i]; ++i){
			var assertTest = require(_self.testsToLoad[i]);

			if (_self.dataObj.get('loglevel') > 2){
				console.log(clc.white.bold(assertTest.name));
				console.log(clc.white(assertTest.info) + '\n');
			}
			assertTest.run(test);
			
			test.finalOutput();

			var reportData = test.finalReportData();
			_self.totalPasses += reportData.passes;
			_self.totalFails += reportData.fails;
		}
	};
};

util.inherits(Runner, events.EventEmitter);
module.exports = Runner;