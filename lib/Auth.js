var cheerio = require('cheerio');
var clc = require('cli-color');
var events = require('events');
var util = require ('util');

var Auth = function(request, dataObj, cookieJar){
	this.request = request;
	this.dataObj = dataObj;
	this.authCookies = cookieJar;
	this.authObj = {};

	this.run = function(){
		console.log(clc.whiteBright.bold('Authenticating...'));
		var _self = this;

		try {
			var urlFile = require(process.cwd()+'/hqa.urls.js');

			for (var field in urlFile.authInfo.fields){
				if (urlFile.authInfo.fields.hasOwnProperty(field)) {
					_self.authObj[field] = urlFile.authInfo.fields[field];
				}
			}

			if (urlFile.authInfo.dynamicSource && urlFile.authInfo.dynamicFields.length > 0){
				var options = {
					url: urlFile.authInfo.dynamicSource,
					jar: _self.authCookies,
					headers : {
						'user-agent': _self.dataObj.get('user-agent')
					}
				};

				_self.request(options, function(error, response, body){
					if (!error && response.statusCode === 200){
						var $ = cheerio.load(body);

						for (var f=0; f<urlFile.authInfo.dynamicFields.length; ++f){
							_self.authObj[urlFile.authInfo.dynamicFields[f]] = $('input[name="'+urlFile.authInfo.dynamicFields[f]+'"]').val();
						}
					} else {
						if (error){
							console.log(clc.redBright('AUTH - ERROR ' + error + ': ' + urlFile.authInfo.dynamicSource));
						}
						if (response && response.statusCode !== 200){
							console.log(clc.redBright('AUTH - HTTP ERROR ' + response.statusCode + ': ' + urlFile.authInfo.dynamicSource));
						}
					}

					_self.emit('dyn-fields', urlFile.authInfo.action, urlFile.authInfo.method || 'POST', _self.authObj);
				});
			} else {
				_self.emit('dyn-fields', urlFile.authInfo.action, urlFile.authInfo.method || 'POST', _self.authObj);
			}
		} catch (e) {
			console.log(clc.redBright('ERROR - CANNOT AUTHENTICATE'));
			process.exit(1);
		}
	};

	this.auth = function(p_action, p_method, p_formData){
		var _self = this,
			authOpt = {
				method: p_method,
				jar: _self.authCookies,
				headers: {
					'user-agent': _self.dataObj.get('user-agent')
				},
				form: p_formData
			};

		_self.request(p_action, authOpt, function(error, response){
			if (!error && response.statusCode === 200){
				_self.emit('auth-done');
			} else {
				if (error){
					console.log(clc.redBright('AUTH - ERROR '+error+': '+p_action));
					process.exit(1);
				}
				if (response && response.statusCode !== 200){
					console.log(clc.redBright('AUTH - HTTP ERROR '+response.statusCode+': '+p_action));
					process.exit(1);
				}
			}
		});
	};

	this.on('dyn-fields', this.auth);
};

util.inherits(Auth, events.EventEmitter);
module.exports = Auth;