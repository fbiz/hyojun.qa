var clc = require('cli-color');
var failMsg = clc.bgRedBright.whiteBright,
	sectionMsg = clc.yellowBright,
	infoMsg = clc.cyan,
	okMsg = clc.green,
	errorMsg = clc.redBright;

/* RESULT */

var TestResult = function(p_message, p_passed, p_testedList, p_failsList){
	return {
		message: p_message || '',
		passed: p_passed || false,
		testedList: p_testedList || [],
		failsList: p_failsList || []
	};
};

/* TEST SECTION */

var TestSection = function(p_name){
	return {
		name: p_name,
		output: []
	};
};

/* TEST */

var Test = function(pageDOM, testConfig){
	this.$ = pageDOM;
	this.testConfig = testConfig;
	this.outputSections = [];
	this.passes = 0;
	this.fails = 0;
};

Test.prototype = {
	resultBadge: function(passed){
		return (passed ? okMsg('PASS') : errorMsg('FAIL'));
	},
	resultLine: function(result) {
		var output = '';

		if (this.testConfig.loglevel > 1){
			output += this.resultBadge(result.passed) + ' ';
			output += infoMsg(result.message);

			if (result.failsList.length) {
				output += ' ' + errorMsg(result.failsList.length + ' failed');
			}

			output += '\n';
		}

		if (this.testConfig.loglevel === 3 && result.failsList.length){
			output += '     '+failMsg(' FAILED ITEMS: ')+'\n';
			for (var f=0; f<result.failsList.length; ++f){
				output += '     - ' + result.failsList[f] + '\n';
			}
		}

		return output;
	},
	resultOutput: function(result){
		if (result.passed) {
			this.passes++;
		} else {
			this.fails++;
		}

		if (result.passed && this.testConfig.failonly) {
			return;
		}

		if (this.outputSections.length === 0){
			this.outputSections.push(new TestSection('Default section'));
		}

		var output = this.resultLine(result);

		if (output !== '') {
			this.outputSections[this.outputSections.length-1].output.push(output);
		}

	},
	finalOutput: function(){
		for (var i=0; i<this.outputSections.length; ++i){
			if (this.outputSections[i].output.length > 0){
				console.log(sectionMsg('+++ '+this.outputSections[i].name+' +++'));

				for (var j=0; j<this.outputSections[i].output.length; ++j){
					console.log(this.outputSections[i].output[j]);
				}
			}
		}
	},
	finalReportData: function(){
		return {
			passes: this.passes,
			fails: this.fails
		};
	},
	section: function(name){
		this.outputSections.push(new TestSection(name));
	},
	assert: function(condition, message){
		this.resultOutput(new TestResult(message, condition === true));
	},
	assertNot: function(condition, message){
		this.resultOutput(new TestResult(message, condition === false));
	},
	assertEquals: function(testval, expected, message){
		this.resultOutput(new TestResult(message, (testval === expected)));
	},
	assertNotEquals: function(testval, expected, message){
		this.resultOutput(new TestResult(message, (testval !== expected)));
	},
	assertExists: function(selector, message){
		var $ = this.$;
		
		var tested = [];
		$(selector).each(function(index, element){
			tested.push($.getPathTo(element));
		});

		this.resultOutput(new TestResult(message, (tested.length > 0), tested));
	},
	assertDoesntExist: function(selector, message){
		var $ = this.$;
		
		var tested = [];
		$(selector).each(function(index, element){
			tested.push($.getPathTo(element));
		});

		this.resultOutput(new TestResult(message, (tested.length === 0), tested, tested));
	},
	assertAttr: function(selector, attribute, values, expectTrue, message){
		var $ = this.$;

		var fails = [];
		var tested = [];
		var testExpect = function(item, element, expectTrue) {
			if (item instanceof RegExp){
				if (item.test($(this).attr(attribute)) !== expectTrue) {
					fails.push($.getPathTo(element));
				}
			} else {
				if (($(this).attr(attribute) === item) !== expectTrue) {
					fails.push($.getPathTo(element));
				}
			}
		};

		$(selector).each(function(index, element) {
			tested.push($.getPathTo(element));
			for (var i=0; i<values.length; ++i){
				testExpect.bind(this)(values[i], element, expectTrue);
			}
		});

		this.resultOutput(new TestResult(message, (fails.length === 0), tested, fails));
	},
	assertAttrExists: function(selector, attribute, message){
		var $ = this.$;

		var fails = [];
		var tested = [];

		$(selector).each(function(index, element){
			tested.push($.getPathTo(element));
			if (!$(element).attr(attribute) && $(element).attr(attribute) !== ''){
				fails.push($.getPathTo(element));
			}
		});

		this.resultOutput(new TestResult(message, (fails.length === 0), tested, fails));
	},
	assertAttrDoesntExist: function(selector, attribute, message){
		var $ = this.$;

		var fails = [];
		var tested = [];

		$(selector).each(function(index, element){
			tested.push($.getPathTo(element));
			if ($(element).attr(attribute) || $(element).attr(attribute) === ''){
				fails.push($.getPathTo(element));
			}
		});

		this.resultOutput(new TestResult(message, (fails.length === 0), tested, fails));
	},
	assertAttrTrue: function(selector, attribute, values, message){
		this.assertAttr(selector, attribute, values, true, message);
	},
	assertAttrFalse: function(selector, attribute, values, message){
		this.assertAttr(selector, attribute, values, false, message);
	}
};

module.exports = Test;