var clc = require('cli-color');
var DOMParser = require('xmldom').DOMParser;
var events = require('events');
var util = require ('util');

var Crawler = function(request){
	this.request = request;
	this.parser = new DOMParser();
	this.sitemapList = [];
	this.crawlList = [];

	this.crawl = function(url) {
		var _self = this;

		_self.request(url, function(error, response, body){
			if (!error && response.statusCode === 200){
				console.log(clc.cyan('# sitemap: ') + url);
			}

			var xml = _self.parser.parseFromString(body, 'text/xml');
			var preparsed = xml.documentElement.textContent.split(/\n|\r|\t/);

			for (var i=0; i<preparsed.length; i++) {
				if (/http(s)?:\/\//i.test(preparsed[i])) {
					if (xml.documentElement.tagName.toLowerCase() === 'sitemapindex') {
						_self.sitemapList.push(preparsed[i].replace(/^\s+|\s+$/g, ''));
					} else {
						var u = preparsed[i].replace(/^\s+|\s+$/g, '');
						_self.crawlList.push({url: u, auth:false});
					}
				}
			}

			if (xml.documentElement.tagName.toLowerCase() === 'sitemapindex') {
				for (var j=0; j<_self.sitemapList.length; j++) {
					_self.crawl(_self.sitemapList[j]);
				}
			} else {
				_self.emit('crawl-done', _self.crawlList);
			}

		});
	};
};

util.inherits(Crawler, events.EventEmitter);
module.exports = Crawler;