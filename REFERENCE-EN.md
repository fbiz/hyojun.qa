# Creating a new test
    // a name for our test
    exports.name = 'My shiny test';
    
    // a description
    exports.info = 'YOU SHALL (NOT) PASS!'; 
    
    exports.run = function(test){
        // ASSERT TESTS GO HERE
    }

# Sublime Text Snippet

Tools > New Snippet > [Paste code] > Save as `hqatest.sublime-snippet`
 
    <snippet>
        <content><![CDATA[
    exports.name = '$1';
    exports.info = '$2';
    
    exports.run = function(test){
        var \$ = test.\$;
        $3
    };
    ]]></content>
    <tabTrigger>hqatest</tabTrigger>
    <scope>source.js</scope>
    <description>New Hyojun.QA Test</description>
    </snippet>

# A simple test
    exports.name = 'Simple test';
    exports.info = 'Example assert tests';
    
    exports.run = function(test){
        test.section('Testing doctype');
        test.assertEquals(test.$.doctype().length, 1, 'one doctype per page');
        
        test.section('Testing links');
        test.assertAttrFalse('a', 'href', ['', '#'], 'links must not be empty or hashes-only');
        test.assertAttrFalse('a', 'href', [/^($|#$)/], 'same as above - using RegExp');
        
        test.section('Testing headers');
        test.assertExists('h1', 'must have h1');
        test.assertAttrExists('h1', 'class', 'h1 must have a class attribute');
        
        var h2 = test.$('h2').length;
        var h1 = test.$('h1').length;
        test.assert((h2>0 && h1>0)||h2==0, 'h2 only if h1');
    }

# Test $ property

### test.$
The cheerio's (jQueryish) instance for the page being tested

# test.$ special method(s)

### test.$.doctype()
Returns an array of strings, representing <!doctype> ocurrences

# Test methods

(all **message** parameters define what will be displayed in output)

### test.section (_String name_)
Defines a new test section (for visual purposes only)

### test.assert (_Boolean condition_, _String message_)
Asserts condition is **strictly** true

### test.assertNot (_Boolean condition_, _String message_)
Asserts condition is _falsey_ (evaluates to false)

### test.assertEquals (_Mixed testval_, _Mixed expected_, _String message_)
Asserts comparison between testval and expected to be **strictly** true

### test.assertNotEquals (_Mixed testval_, _Mixed expected_, _String message_)
Asserts comparison between testval and expected to be **strictly** false

### test.assertExists (_String selector_, _String message_)
Asserts selector is found in DOM

### test.assertDoesntExist (_String selector_, _String message_)
Asserts selector is NOT found in DOM

### test.assertAttr (_String selector_, _String attribute_, _Array values_, _Boolean expectTrue_, _String message_)
Asserts all values in selector's attribute evaluates to true if expectTrue is true

Asserts all values in selector's attribute evaluates to false if expectTrue is false

#### test.assertAttrTrue (_String selector_, _String attribute_, _Array values_, _String message_)

#### test.assertAttrFalse (_String selector_, _String attribute_, _Array values_, _String message_)

Shortcut methods for assertAttr

### test.assertAttrExists (_String selector_, _String attribute_, _String message_)
Asserts selector's attribute exists

### test.assertAttrDoesntExist (_String selector_, _String attribute_, _String message_)
Asserts selector's attribute doesn't exist
