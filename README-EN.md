# hyojun.qa

html code quality assurance testing tool

## test reference:
[Here](https://bitbucket.org/fbiz/hyojun.qa/src/HEAD/REFERENCE.md)

## needs:
nodejs - [http://nodejs.org](http://nodejs.org)

## installation:
clone this repo

`cd hyojun.qa` to access directory

`npm install .` to download dependencies

## basic usage:
    $ ./hyojun.qa --url PAGE_URL [--auth [--crawl [--loglevel VALUE [--failonly [--test=TEST_PATHS]]]]]

**Windows:** `node hyojun.qa` instead of `./hyojun.qa`

**Linux:** some distros call node's executable `nodejs`; if that's your case, a symbolic link will be needed: `sudo ln -s /usr/bin/nodejs /usr/bin/node`

#### arguments
`-u` or `--url PAGE_URL` - any URL beginning with http:// or https:// (omit PAGE\_URL to use URLs listed in _hqa.urls.js_)

`-a` or `--auth` - get authentication cookie using authInfo object at _hqa.urls.js_

`-c` or `--crawl` - fetches sitemap.xml from page domain and run tests in all URLs found

`-l` or `--loglevel VALUE` - show info based on VALUE: 0 - silent mode, 1 - results only (default), 2 - displays all tests and result, 3 - full log with failed elements when applicable

`-s` or `--silent` - alias for `--loglevel 0`

`-f` or `--failonly` - show only fails, omitting passes

`-t` or `--test TEST_PATHS` - comma-separated testfile paths - eg. `test_a.js, test_b.js, ...` (if omitted, uses tests listed in _hqa.config.js_ - if config file not found, looks for .js files in _hqa-tests_ directory)

## hqa.config.js file:

defines an array named `testOrder` which contains tests to be executed

    exports.testOrder = [
        "./mytests/test_c.js",
        "./mytests/test_3.js",
        "./mytests/test_p.js",
        "./mytests/test_o.js"
    ];

## hqa.urls.js file:

this file defines two things: authentication info and URLs to test

**authentication info**: defines an object named `authInfo` which contains the following structure:
    
    exports.authInfo = {
        action  : "[LOGIN-FORM-ACTION-URL]",
        method  : "[GET OR POST]",
        fields  : {
            "[FIELD-NAME]" : "[FIELD-VALUE]",
            "[FIELD-NAME]" : "[FIELD-VALUE]"
        },
        // array which lists inputs which values are defined dynamically via back-end (e.g. _hidden_ fields)
        dynamicFields : [
            "[FIELD-NAME]"
        ],
        // string with the URL which contains the form (so the dynamic fields - and values - can be fetched)
        dynamicSource: "[FORM-URL]"
    };

**URLs to test**: defines an array named `urlList` which contains the URLs (and info related to them) to apply the tests

    exports.urlList = [
        {
            url: "http://www.domain.tld",
            auth: false,
            headers: {
                user-agent: 'chrome-windows',
                other-header: 'value'
            },
            postData: {
                key: 'value',
                key2: 'value2'
            }
        },
        {
            url: "http://www.domain.tld/internal-page",
            auth: false
        },
        {
            url: "http://www.domain.tld/other-page.html",
            auth: false
        },
        {
            url: "http://www.domain.tld/logged-area",
            auth: true
        },
        {
            url: "http://www.domain.tld/logged-area",
            auth: false // test URL also when not logged
        },
    ];

## data directory

contains JSON files, such as `hqa.user-agents.json`, which contains common user-agents aliases to be used in each request and `hqa.defaults.json`, which contains default values to be used in each request.

## How to contribute

All help is welcomed and to guarantee that the project evolves consistently, there are some items to be considered:

* Always open a issue and discuss before making changes to the code;
* Fork the repository and create a branch with the version name where the issue is indicated, eg.: 
`1.1.2`;
* When cloning it to your machine, link the project hook in your repo:

    ```
    $ ln -s .hooks/pre-commit .git/hooks/pre-commit
    ```

    This will run `grunt test` everytime you commit to guarantee that no mistakes goes to bitbucket.

* All files must pass jshint rules defined and `~/.jshintrc`;
* The documentation must be updated;
* When commiting, use hooks to point the issue you solved: `resolve #9`;
* After finish, make a pull request of the branch you worked;