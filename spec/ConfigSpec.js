/*jshint maxstatements: false, maxlen: false */
/*global describe, expect, it */

var Config = require('../lib/Config');

describe('Config', function() {
	var config;

	it('should initialize loading the default config file (./hqa-config.js)', function() {
		config = new Config();
		config.init();
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
	});
	it('should search a custom config file that doesn\'t exist and fallback to the default location', function() {
		config = new Config();
		config.init('teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('./teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('../teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('../../teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('teste/teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('./teste/teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('../teste/teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
		config = new Config();
		config.init('../../teste/teste.js');
		expect(config.get('path')).toMatch(/hqa\.config\.js$/);
	});
	it('should search and find a custom config file', function() {
		config = new Config();
		config.init('./spec/test-config/custom-name.config.js');
		expect(config.get('path')).toMatch(/spec\/test-config\/custom-name\.config\.js$/);
	});

	it('should search and find a custom config file using relative path', function() {
		config = new Config();
		config.init('./spec/../spec/test-config/custom-name.config.js');
		expect(config.get('path')).toMatch(/spec\/test-config\/custom-name\.config\.js$/);
	});

	it('should search default "hqa.urls.js" file', function() {
		config = new Config();
		config.init('./spec/../spec/test-config/custon-name.config.js');
		expect(config.get('urlPath')).toMatch(/hqa\.urls\.js$/);
		expect(config.get('urlList')[0].url).toEqual('http://www.fbiz.com.br/');
	});
	it('should search custom "hqa.test-urls.js" file', function() {
		config = new Config();
		config.init('./spec/../spec/test-config/url-external.config.js');
		expect(config.get('urlPath')).toMatch(/hqa\.test-urls\.js$/);
		expect(config.get('urlList')[0].url).toEqual('http://www.fbiz.com.br/trabalhos');
	});
	it('should load embedded url list from config file', function() {
		config = new Config();
		config.init('./spec/../spec/test-config/url-embedded.config.js');
		expect(config.get('urlPath')).toBeNull();
		expect(config.get('urlList')[0].url).toEqual('http://www.fbiz.com.br/sobre');
	});

	it('should search for all files inside default tests folder (hqa-tests)', function() {
		var tests = null;
		config = new Config();
		config.init('./spec/../spec/test-config/custom-name.config.js');
		expect(config.get('testPath')).toMatch(/hqa-tests$/);
		tests = config.get('testsToLoad');
		expect(tests && tests.length > 0).toBeTruthy();
		expect(tests[0]).toMatch(/attribs\.js$/);
	});
	it('should search tests inside a custom folder', function() {
		var tests = null;
		config = new Config();
		config.init('./spec/../spec/test-config/test-folder.config.js');
		expect(config.get('testPath')).toMatch(/test-config\/hqa-tests$/);
		tests = config.get('testsToLoad');
		expect(tests && tests.length > 0).toBeTruthy();
		expect(tests[0]).toMatch(/hello-universe\.js$/);
	});
	it('should have the test list embedded inside the config file', function() {
		var tests = null;
		config = new Config();
		config.init('./spec/../spec/test-config/test-list.config.js');
		expect(config.get('testPath')).toBeNull();
		tests = config.get('testsToLoad');
		expect(tests && tests.length > 0).toBeTruthy();
		expect(tests[0]).toMatch(/hello-world\.js$/);
	});
	it('should receive the test list via parameter', function() {
		var tests = null;
		config = new Config();
		config.init('./spec/../spec/test-config/custom-name.config.js');
		config.checkTestsLocation(['./spec/test-config/hqa-tests/hello-world.js']);
		expect(config.get('testPath')).toBeNull();
		tests = config.get('testsToLoad');
		expect(tests && tests.length > 0).toBeTruthy();
		expect(tests[0]).toMatch(/test-config\/hqa-tests\/hello-world\.js$/);
	});
});


