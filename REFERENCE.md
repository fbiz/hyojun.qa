[english version](https://bitbucket.org/fbiz/hyojun.qa/src/HEAD/REFERENCE-EN.md)

# Criando um teste
    // um nome para nosso teste
    exports.name = 'Meu teste';
    
    // Uma descrição
    exports.info = 'Testa coisas'; 
    
    exports.run = function(test){
        // TESTES DE VALIDAÇÃO AQUI
    }

# Sublime Text Snippet

Tools > New Snippet > [Cole o código abaixo] > Salve como `hqatest.sublime-snippet`
 
    <snippet>
        <content><![CDATA[
    exports.name = '$1';
    exports.info = '$2';
    
    exports.run = function(test){
        var \$ = test.\$;
        $3
    };
    ]]></content>
    <tabTrigger>hqatest</tabTrigger>
    <scope>source.js</scope>
    <description>New Hyojun.QA Test</description>
    </snippet>

# Um teste simples
    exports.name = 'Teste simples';
    exports.info = 'Validações de exemplo';
    
    exports.run = function(test){
        test.section('Testando doctype');
        test.assertEquals(test.$.doctype().length, 1, 'um doctype por pagina');
        
        test.section('Testando links');
        test.assertAttrFalse('a', 'href', ['', '#'], 'links não devem ser vazios ou apenas #');
        test.assertAttrFalse('a', 'href', [/^($|#$)/], 'o mesmo que o teste acima - utilizando RegExp');
        
        test.section('Testando headers');
        test.assertExists('h1', 'deve existir h1');
        test.assertAttrExists('h1', 'class', 'h1 deve ter um atributo class');
        
        var h2 = test.$('h2').length;
        var h1 = test.$('h1').length;
        test.assert((h2>0 && h1>0)||h2==0, 'h2 somente se houver h1');
    }

# Propriedade $

### test.$
A instância do cheerio (baseado em jQuery) para a página sendo testada

# método(s) especial(is) do test.$

### test.$.doctype()
Retorna uma array de strings, representando ocorrências de <!doctype>

# Métodos de test

(todos os parâmetros **message** definem o que será mostrado no output)

### test.section (_String name_)
Define uma nova seção de testes (apenas com propósitos visuais / de organização)

### test.assert (_Boolean condition_, _String message_)
Valida se _condition_ é **estritamente** true

### test.assertNot (_Boolean condition_, _String message_)
Valida se _condition_ é falsa (eval para false)

### test.assertEquals (_Mixed testval_, _Mixed expected_, _String message_)
Valida se a comparação entre _testval_ e _expected_ é **estritamente** true

### test.assertNotEquals (_Mixed testval_, _Mixed expected_, _String message_)
Valida se a comparação entre _testval_ e _expected_ é **estritamente** false

### test.assertExists (_String selector_, _String message_)
Valida se _selector_ está contido no DOM

### test.assertDoesntExist (_String selector_, _String message_)
Valida se _selector_ NÃO está contido no DOM

### test.assertAttr (_String selector_, _String attribute_, _Array values_, _Boolean expectTrue_, _String message_)
Valida se todos os valores nos _attribute_ de _selector_ dão eval para true se _expectTrue_ for true

Valida se todos os valores nos _attribute_ de _selector_ dão eval para true se _expectTrue_ for true

#### test.assertAttrTrue (_String selector_, _String attribute_, _Array values_, _String message_)

#### test.assertAttrFalse (_String selector_, _String attribute_, _Array values_, _String message_)

Métodos de atalho para assertAttr

### test.assertAttrExists (_String selector_, _String attribute_, _String message_)
Valida se o _attribute_ em todas as ocorrências de _selector_ existe

### test.assertAttrDoesntExist (_String selector_, _String attribute_, _String message_)
Valida se o _attribute_ em todas as ocorrências de _selector_ não existe
