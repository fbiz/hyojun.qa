# hyojun.qa

ferramenta de testes de controle de qualidade em código HTML

[english version](https://bitbucket.org/fbiz/hyojun.qa/src/HEAD/README-EN.md)

[apresentação no speakerdeck](https://speakerdeck.com/renatodarrigo/hyojun-dot-qa)

## referência para testes:
[aqui](https://bitbucket.org/fbiz/hyojun.qa/src/HEAD/REFERENCE.md)

## requisito:
nodejs - [http://nodejs.org](http://nodejs.org)

## instalação:
clone neste repositório

`cd hyojun.qa` para acessar o diretório

`npm install .` para fazer download das dependências

## uso básico:
    $ ./hyojun.qa --url PAGE_URL [--auth [--crawl [--loglevel VALUE [--failonly [--test=TEST_PATHS]]]]]

**Windows:** `node hyojun.qa` no lugar de `./hyojun.qa`

**Linux:** algumas versões chamam o executável do node de `nodejs`; se for o caso, será necessário criar um link simbólico: `sudo ln -s /usr/bin/nodejs /usr/bin/node`

#### argumentos
`-u` ou `--url PAGE_URL` - qualquer URL que inicie com http:// ou https:// (omita PAGE\_URL para usar as URLs listadas em _hqa.urls.js_)

`-a` ou `--auth` - obtém cookie de autenticação utilizando os dados do objeto authInfo em _hqa.urls.js_

`-c` ou `--crawl` - busca o arquivo sitemap.xml do domínio e roda testes em todas as URLs encontradas

`-l` ou `--loglevel VALUE` - exibe informações de acordo com o VALUE: 0 - modo silencioso, 1 - apenas resultados (padrão), 2 - mostra todos os testes e resultados, 3 - log completo com elementos que falharam quando aplicável

`-s` ou `--silent` - atalho para `--loglevel 0`

`-f` ou `--failonly` - mostra apenas falhas, omitindo os sucessos

`-t` ou `--test TEST_PATHS` - caminhos de arquivos de teste separados por vírgula - ex. `test_a.js, test_b.js, ...` (se omitido, usa os testes listados em _hqa.config.js_ - se o arquivo de configuração não for encontrado, busca arquivos .js no diretório _hqa-tests_)

## arquivo hqa.config.js:

define uma array de nome `testOrder`, que contém os testes a serem executados

    exports.testOrder = [
        "./mytests/test_c.js",
        "./mytests/test_3.js",
        "./mytests/test_p.js",
        "./mytests/test_o.js"
    ];

## arquivo hqa.urls.js:

este arquivo define duas coisas: dados de autenticação e URLs a serem testadas

**dados de autenticação**: define um objeto de nome `authInfo`, que tem a seguinte estrutura:
    
    exports.authInfo = {
        action  : "[LOGIN-FORM-ACTION-URL]",
        method  : "[GET OU POST]",
        fields  : {
            "[NOME-DO-CAMPO]" : "[VALOR-DO-CAMPO]",
            "[NOME-DO-CAMPO]" : "[VALOR-DO-CAMPO]"
        },
        // array que lista inputs cujos valores são definidos dinamicamente via back-end (ex. campos _hidden_)
        dynamicFields : [
            "[NOME-DO-CAMPO]"
        ],
        // string com a URL que contém o form (para que os campos dinâmicos - e seus valores - possam ser obtidos)
        dynamicSource: "[FORM-URL]"
    };

**URLs a serem testadas**: define uma array chamada `urlList`, que contém as URLs (e outras informações relacionadas a elas) onde serão aplicados os testes

    exports.urlList = [
        {
            url: "http://www.domain.tld",
            auth: false,
            headers: {
                user-agent: 'chrome-windows',
                other-header: 'value'
            },
            postData: {
                key: 'value',
                key2: 'value2'
            }
        },
        {
            url: "http://www.domain.tld/internal-page",
            auth: false
        },
        {
            url: "http://www.domain.tld/other-page.html",
            auth: false
        },
        {
            url: "http://www.domain.tld/logged-area",
            auth: true
        },
        {
            url: "http://www.domain.tld/logged-area",
            auth: false // testando mesma URL sem estar autenticado
        },
    ];

## diretório data:

o diretório contém arquivos JSON, tais como `hqa.user-agents.json`, que contém atalhos de user agents comuns a serem utilizados nas requisições às páginas e `hqa.defaults.json` que contém os valores padrão a serem enviados nas requisições às páginas.

## Como contribuir

Toda ajuda é bem vinda e para garantir que o projeto evolua de uma forma consistente, existem alguns pontos que devem ser levados em consideração:

* Sempre abra uma issue e discuta antes de fazer alterações no código;
* Faça um fork e crie um branch com o nome da version que aquela issue se encontra, ex.: `1.1.2`;
* Quando fizer o clone na sua máquina, faça um link do hook do projeto para seu repositório:

    ```
    $ ln -s .hooks/pre-commit .git/hooks/pre-commit
    ```

    Isso fará com que `grunt test` seja rodado toda vez que você fizer um commit para garantir que nenhum erro vá para o bitbucket.

* Todos os arquivos devem passar nas regras do jshint definidas em `~/.jshintrc`;
* A documentação deve estar atualizada;
* Faça os commits apontando a issue que você resolveu, utilizando os hooks: `resolve #9`;
* Após terminar, faça um pull request do branch que você trabalhou;