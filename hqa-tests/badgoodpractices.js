/*jshint maxstatements: false, maxlen: false */

exports.name = 'Bad/Good practices testing';
exports.info = 'Test semantic and structural good and bad practices';

exports.run = function(test){
	var $ = test.$;
	
	test.section('Structure');

	test.assert($.doctype().length === 1, 'must have doctype - and only one');

	var inlineElements = ['b', 'big', 'i', 'small', 'tt', 'abbr', 'acronym', 'cite', 'code', 'dfn', 'em', 'kbd', 'strong', 'samp', 'var', 'a', 'bdo', 'br', 'img', 'map', 'object', 'q', 'script', 'span', 'sub', 'sup', 'button', 'input', 'label', 'select', 'textarea'];
	var blockElements = ['address', 'article', 'aside', 'audio', 'blockquote', 'canvas', 'dd', 'div', 'dl', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'header', 'hgroup', 'hr', 'noscript', 'ol', 'output', 'p', 'pre', 'section', 'table', 'tfoot', 'ul', 'video'];
	var testSelectors = [];
	for (var i = 0; i < inlineElements.length; ++i){
		for (var b = 0; b < blockElements.length; ++b){
			testSelectors.push(inlineElements[i]+' '+blockElements[b]);
		}
	}
	var selectors = testSelectors.join(', ');
	test.assertDoesntExist(selectors, 'inline elements must not contain block-level elements');

	test.section('Semantic');

	var h1 = $('h1').length;
	var h2 = $('h2').length;
	test.assert((h2>0 && h1>0)||h2===0, 'h2 can only exist if h1 exists');

	test.assertDoesntExist('blink', 'blink element must not be used');

	test.assertDoesntExist('marquee', 'marquee element must not be used');

	test.assertAttrFalse('a', 'href', [/^($|#$|\s+$|javascript:)/i], 'links must point somewhere');

};