/*jshint maxstatements: false, maxlen: false */

exports.name = 'Attributes testing';
exports.info = 'Test required and bad/deprecated attributes in various tags';

exports.run = function(test){

	test.section('*');
	test.assertAttrDoesntExist('*', 'border', 'must not have border - use CSS instead');
	test.assertAttrDoesntExist('*', 'style', 'must not have style - use stylesheet file instead');

	test.section('<LINK>');
	test.assertAttrExists('link', 'href', 'must have href');
	test.assertAttrExists('link', 'rel', 'must have rel');

	test.section('<BODY>');
	test.assertAttrDoesntExist('body', 'alink', 'must not have alink - use CSS instead');
	test.assertAttrDoesntExist('body', 'background', 'must not have background - use CSS instead');
	test.assertAttrDoesntExist('body', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('body', 'link', 'must not have link - use CSS instead');
	test.assertAttrDoesntExist('body', 'text', 'must not have text - use CSS instead');
	test.assertAttrDoesntExist('body', 'vlink', 'must not have vlink - use CSS instead');

	test.section('<H*>');
	test.assertAttrDoesntExist('h1', 'align', 'h1 must not have align - use CSS instead');
	test.assertAttrDoesntExist('h2', 'align', 'h2 must not have align - use CSS instead');
	test.assertAttrDoesntExist('h3', 'align', 'h3 must not have align - use CSS instead');
	test.assertAttrDoesntExist('h4', 'align', 'h4 must not have align - use CSS instead');
	test.assertAttrDoesntExist('h5', 'align', 'h5 must not have align - use CSS instead');
	test.assertAttrDoesntExist('h6', 'align', 'h6 must not have align - use CSS instead');

	test.section('<P>');
	test.assertAttrDoesntExist('p', 'align', 'must not have align - use CSS instead');

	test.section('<HR>');
	test.assertAttrDoesntExist('hr', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('hr', 'color', 'must not have color - use CSS instead');
	test.assertAttrDoesntExist('hr', 'noshade', 'must not have noshade - use CSS instead');
	test.assertAttrDoesntExist('hr', 'size', 'must not have size - use CSS instead');
	test.assertAttrDoesntExist('hr', 'width', 'must not have width - use CSS instead');

	test.section('<PRE>');
	test.assertAttrDoesntExist('pre', 'cols', 'must not have cols - use CSS instead');
	test.assertAttrDoesntExist('pre', 'width', 'must not have width - use CSS instead');
	test.assertAttrDoesntExist('pre', 'wrap', 'must not have wrap - use CSS instead');

	test.section('<OL>');
	test.assertAttrDoesntExist('ol', 'compact', 'must not have compact - use CSS instead');

	test.section('<UL>');
	test.assertAttrDoesntExist('ul', 'compact', 'must not have compact - use CSS instead');
	test.assertAttrDoesntExist('ul', 'type', 'must not have type - use CSS instead');

	test.section('<LI>');
	test.assertAttrDoesntExist('li', 'type', 'must not have type - use CSS instead');

	test.section('<DL>');
	test.assertAttrDoesntExist('dl', 'compact', 'must not have compact - use CSS instead');

	test.section('<DD>');
	test.assertAttrDoesntExist('dd', 'nowrap', 'must not have nowrap - use CSS instead');

	test.section('<DIV>');
	test.assertAttrDoesntExist('div', 'align', 'must not have align - use CSS instead');

	test.section('<A>');
	test.assertAttrExists('a', 'href', 'must have href');

	test.section('<IMG>');
	test.assertAttrExists('img', 'alt', 'must have alt');
	test.assertAttrExists('img', 'src', 'must have src');
	test.assertAttrDoesntExist('img', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('img', 'border', 'must not have border - use CSS instead');
	test.assertAttrDoesntExist('img', 'hspace', 'must not have hspace - use CSS instead');
	test.assertAttrDoesntExist('img', 'vspace', 'must not have vspace - use CSS instead');

	test.section('<IFRAME>');
	test.assertAttrDoesntExist('iframe', 'align', 'must not have align - obsolete');

	test.section('<TABLE>');
	test.assertAttrDoesntExist('table', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('table', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('table', 'border', 'must not have border - use CSS instead');
	test.assertAttrDoesntExist('table', 'cellpadding', 'must not have cellpadding - use CSS instead');
	test.assertAttrDoesntExist('table', 'cellspacing', 'must not have cellspacing - use CSS instead');
	test.assertAttrDoesntExist('table', 'frame', 'must not have frame - use CSS instead');
	test.assertAttrDoesntExist('table', 'rules', 'must not have rules - use CSS instead');
	test.assertAttrDoesntExist('table', 'summary', 'must not have summary - use <caption> instead');
	test.assertAttrDoesntExist('table', 'width', 'must not have width - use CSS instead');

	test.section('<CAPTION>');
	test.assertAttrDoesntExist('caption', 'align', 'must not have align - use CSS instead');

	test.section('<TBODY>');
	test.assertAttrDoesntExist('tbody', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('tbody', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('tbody', 'char', 'must not have char - obsolete');
	test.assertAttrDoesntExist('tbody', 'charoff', 'must not have charoff - obsolete');
	test.assertAttrDoesntExist('tbody', 'valign', 'must not have valign - use CSS instead');

	test.section('<THEAD>');
	test.assertAttrDoesntExist('thead', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('thead', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('thead', 'char', 'must not have char - obsolete');
	test.assertAttrDoesntExist('thead', 'charoff', 'must not have charoff - obsolete');
	test.assertAttrDoesntExist('thead', 'valign', 'must not have valign - use CSS instead');

	test.section('<TFOOT>');
	test.assertAttrDoesntExist('tfoot', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('tfoot', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('tfoot', 'char', 'must not have char - obsolete');
	test.assertAttrDoesntExist('tfoot', 'charoff', 'must not have charoff - obsolete');
	test.assertAttrDoesntExist('tfoot', 'valign', 'must not have valign - use CSS instead');

	test.section('<TR>');
	test.assertAttrDoesntExist('tr', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('tr', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('tr', 'char', 'must not have char - obsolete');
	test.assertAttrDoesntExist('tr', 'charoff', 'must not have charoff - obsolete');
	test.assertAttrDoesntExist('tr', 'valign', 'must not have valign - use CSS instead');

	test.section('<TD>');
	test.assertAttrDoesntExist('td', 'abbr', 'must not have abbr - use title instead');
	test.assertAttrDoesntExist('td', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('td', 'axis', 'must not have axis - obsolete');
	test.assertAttrDoesntExist('td', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('td', 'char', 'must not have char - obsolete');
	test.assertAttrDoesntExist('td', 'charoff', 'must not have charoff - obsolete');
	test.assertAttrDoesntExist('td', 'scope', 'must not have scope - obsolete');
	test.assertAttrDoesntExist('td', 'valign', 'must not have valign - use CSS instead');

	test.section('<TH>');
	test.assertAttrDoesntExist('th', 'abbr', 'must not have abbr - use title instead');
	test.assertAttrDoesntExist('th', 'align', 'must not have align - use CSS instead');
	test.assertAttrDoesntExist('th', 'axis', 'must not have axis - obsolete');
	test.assertAttrDoesntExist('th', 'bgcolor', 'must not have bgcolor - use CSS instead');
	test.assertAttrDoesntExist('th', 'char', 'must not have char - obsolete');
	test.assertAttrDoesntExist('th', 'charoff', 'must not have charoff - obsolete');
	test.assertAttrDoesntExist('th', 'valign', 'must not have valign - use CSS instead');

	test.section('<FORM>');
	test.assertAttrExists('form', 'action', 'must have action');
	test.assertAttrDoesntExist('form', 'accept', 'must not have accept - obsolete');

	test.section('<INPUT>');
	test.assertAttrExists('input', 'type', 'must have type');
	test.assertAttrDoesntExist('input', 'accesskey', 'must not have accesskey - use <label accesskey> instead');
	test.assertAttrDoesntExist('input', 'usemap', 'must not have usemap - obsolete');
};