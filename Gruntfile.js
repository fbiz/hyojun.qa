module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-jasmine-node');

	grunt.initConfig({
		jshint: {
			options: {
				jshintrc: './.jshintrc'
			},
			lib: [
				'lib/**/*.js',
				'data/**/*.js',
			],
			'hqa-tests': [
				'hqa-tests/**/*.js'
			],
			grunt: [
				'Gruntfile.js',
			],
			data: [
				'hqa.config.js',
				'hqa.urls.js'
			],
			cli: [
				'hyojun.qa'
			]
		},
		jasmine_node: {
			options: {
				forceExit: true,
				match: '.',
				matchall: false,
				extensions: 'js',
				specNameMatcher: 'spec'
			},
			all: ['spec/']
		},
		watch: {
			files: ['<%= jshint.files %>'],
			tasks: ['jshint']
		}
	});


	grunt.registerTask('test', ['jshint', 'jasmine_node']);

	grunt.registerTask('default', ['test']);

};
